package org.soen387.domain.model.notification;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.challenge.IChallenge;
import org.soen387.domain.model.checkerboard.ICheckerBoard;
import org.soen387.domain.model.notification.tdg.NotificationFinder;
import org.soen387.domain.model.player.IPlayer;

public class NotificationFactory {	
	// for a game
	public static Notification createNew (long id, IPlayer recipient, boolean seen, NotificationStatus status, ICheckerBoard notificationType) throws SQLException, MapperException {
		Notification n = new Notification (id, 1, recipient, seen, status, notificationType.getId());
		UoW.getCurrent().registerNew(n);
		return n;
	}
	
	public static Notification createNew (NotificationStatus status, ICheckerBoard notificationType, IPlayer recipient) throws SQLException, MapperException {
		return createNew (NotificationFinder.getMaxId(), recipient, false, status, notificationType);
	}	
	
	// for a challenge
	public static Notification createNew (long id, IPlayer recipient, boolean seen, NotificationStatus status, IChallenge notificationType) throws SQLException, MapperException {
		Notification n = new Notification (id, 1, recipient, seen, status, notificationType.getId());
		UoW.getCurrent().registerNew(n);
		return n;
	}
	
	public static Notification createNew (NotificationStatus status, IChallenge notificationType, IPlayer recipient) throws SQLException, MapperException {
		return createNew (NotificationFinder.getMaxId(), recipient, false, status, notificationType);
	}
	
	public static Notification createClean (long id, int version, NotificationStatus status, long notificationType, IPlayer recipient, boolean seen) {
		Notification n = new Notification (id, version, recipient, seen, status, notificationType);
		UoW.getCurrent().registerClean(n);
		return n;
	}
}
