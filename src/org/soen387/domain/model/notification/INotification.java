package org.soen387.domain.model.notification;

import org.soen387.domain.model.player.IPlayer;

public interface INotification {

	public abstract Long getId();

	public abstract long getVersion();

	public abstract void setVersion(int version);

	public abstract boolean getSeen();

	public abstract void setSeen(boolean seen);

	public abstract IPlayer getRecipient();

	public abstract void setRecipient(IPlayer recipient);

	public abstract long getType();

	public abstract void setType(long type);

	public abstract void setStatus(NotificationStatus status);

	public abstract NotificationStatus getStatus();


}