package org.soen387.domain.model.notification;

public enum NotificationStatus {
	Turn,
	Tied,
	Won,
	Conceded,
	Loss,
	Started,
	Issued,
	Accepted,
	Refused;
	
	public int getId() { return this.ordinal(); }
}
