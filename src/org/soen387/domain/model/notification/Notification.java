package org.soen387.domain.model.notification;

import org.dsrg.soenea.domain.DomainObject;
import org.soen387.domain.model.player.IPlayer;

public class Notification extends DomainObject<Long> implements INotification {
	long id;
	int version;
	boolean seen;
	IPlayer recipient; 
	long type;
	NotificationStatus status;
	
	public Notification (long id, int version, IPlayer recipient, boolean seen, NotificationStatus status, long type) {
		super(id, version);
		this.id = id;
		this.version = version;
		this.recipient = recipient;
		this.seen = seen;
		this.status = status;
		this.type = type;
	}
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public long getVersion() {
		return version;
	}

	@Override
	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public boolean getSeen() {
		return seen;
	}

	@Override
	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	@Override
	public IPlayer getRecipient() {
		return recipient;
	}

	@Override
	public void setRecipient(IPlayer recipient) {
		this.recipient = recipient;
	}
	
	@Override
	public long getType() {
		return type;
	}
	
	@Override
	public void setType(long type) {
		this.type = type;
	}
	
	@Override
	public NotificationStatus getStatus() {
		return status;
	}

	@Override
	public void setStatus(NotificationStatus status) {
		this.status = status;
	}
}
