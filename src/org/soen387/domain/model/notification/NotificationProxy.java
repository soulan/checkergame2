package org.soen387.domain.model.notification;

import java.sql.SQLException;

import org.dsrg.soenea.domain.DomainObjectCreationException;
import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.proxy.DomainObjectProxy;
import org.soen387.domain.model.notification.mapper.NotificationInputMapper;
import org.soen387.domain.model.player.IPlayer;

public class NotificationProxy extends DomainObjectProxy<Long, Notification> implements INotification {
	long id;
	
	public NotificationProxy(Long id) {
		super(id);
	}
	
	public Notification getInner() {
		try {
			return NotificationInputMapper.find(id);
		} catch (Exception e) {
			// It better be here! That null won't go over well!
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void setVersion(int version) {
		getInner().setVersion(version);
	}

	@Override
	public boolean getSeen() {
		return getInner().getSeen();
	}

	@Override
	public void setSeen(boolean seen) {
		getInner().setSeen(seen);
	}

	@Override
	public IPlayer getRecipient() {
		return getInner().getRecipient();
	}

	@Override
	public void setRecipient(IPlayer recipient) {
		getInner().setRecipient(recipient);
	}

	@Override
	public long getType() {
		return getInner().getType();
	}

	@Override
	public void setType(long type) {
		getInner().setType(type);
	}

	@Override
	public void setStatus(NotificationStatus status) {
		getInner().setStatus(status);
	}

	@Override
	public NotificationStatus getStatus() {
		return getInner().getStatus();
	}

	@Override
	protected Notification getFromMapper(Long id) throws MapperException,
			DomainObjectCreationException {
		Notification n = null;
		try {
			n = NotificationInputMapper.find(id);
		} catch (SQLException e){
			throw new MapperException(e.getMessage());
		}
		return n;
	}
	
}
