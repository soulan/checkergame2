package org.soen387.domain.model.notification.tdg;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.dsrg.soenea.service.threadLocal.DbRegistry;

public class NotificationTDG {
	public static final String TABLE_NAME = "Notification";
	public static final String COLUMNS = "id, version, recipient, seen, status, type ";
	public static final String TRUNCATE_TABLE = "TRUNCATE TABLE  " + TABLE_NAME + ";";
	public static final String DROP_TABLE = "DROP TABLE  " + TABLE_NAME + ";";
	public static final String CREATE_TABLE ="CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" 
			+ "id BIGINT, "
			+ "version int, "
			+ "recipient BIGINT, "
			+ "seen VARCHAR(32), "
			+ "status TINYINT, "
			+ "type BIGINT"
			+ "PRIMARY KEY(id), "
			+ "INDEX(recipient) "
			+ ");";
	
	public static void createTable() throws SQLException {
		Connection con = DbRegistry.getDbConnection();
		Statement update = con.createStatement();
		update.execute(CREATE_TABLE);
	}

	public static void dropTable() throws SQLException {
		Connection con = DbRegistry.getDbConnection();
		Statement update = con.createStatement();
		update.execute(TRUNCATE_TABLE);
		update = con.createStatement();
		update.execute(DROP_TABLE);
	}
	
	
	public static final String INSERT = "INSERT INTO " + TABLE_NAME + " (" + COLUMNS + ") "
			+ "VALUES (?,?,?,?,?,?);";
	public static int insert(long id, int version, long recipient, String seen, int status, long type) throws SQLException {
		Connection con = DbRegistry.getDbConnection();
		PreparedStatement ps = con.prepareStatement(INSERT);
		ps.setLong(1,id);
		ps.setInt(2,version);
		ps.setLong(3,recipient);
		ps.setString(4,seen);
		ps.setInt(5,status);
		ps.setLong(6, type);
		return ps.executeUpdate();
	}
	
	public static final String UPDATE = "UPDATE " + TABLE_NAME + " set version = version+1, recipient=?, seen=?, status=?, type=? "
			+ " WHERE id=? AND version=?;";
	public static int update(long id, int version, long recipient, String seen, int status, long type) throws SQLException {
		Connection con = DbRegistry.getDbConnection();
		PreparedStatement ps = con.prepareStatement(UPDATE);
		ps.setLong(1,recipient);
		ps.setString(2,seen);
		ps.setInt(3,status);
		ps.setLong(4, type);
		ps.setLong(5,id);
		ps.setInt(6,version);
		return ps.executeUpdate();
	}
	
	public static final String DELETE = "DELETE FROM " + TABLE_NAME + " "
			+ "WHERE id=? AND version=?;";
	public static int delete(long id, int version) throws SQLException {
		Connection con = DbRegistry.getDbConnection();
		PreparedStatement ps = con.prepareStatement(DELETE);
		ps.setLong(1,id);
		ps.setInt(2,version);
		return ps.executeUpdate();
	}
}
