package org.soen387.domain.model.notification.mapper;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.mapper.IOutputMapper;
import org.dsrg.soenea.domain.mapper.LostUpdateException;
import org.soen387.domain.model.notification.Notification;
import org.soen387.domain.model.notification.tdg.NotificationTDG;

public class NotificationOutputMapper implements IOutputMapper<Long, Notification> {

	@Override
	public void insert(Notification d) throws MapperException {
		try {
			NotificationTDG.insert(d.getId(), (int) d.getVersion(), d.getRecipient().getUser().getId(), String.valueOf(d.getSeen()), d.getStatus().getId(), d.getType());
		} catch (SQLException e)
		{
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
	} 

	@Override
	public void update(Notification d) throws MapperException {
		try {
			int count = NotificationTDG.update(d.getId(), (int) d.getVersion(), d.getRecipient().getUser().getId(), String.valueOf(d.getSeen()), d.getStatus().getId(), d.getType());
			if(count==0) throw new LostUpdateException("Lost Update editing Notification with id " + d.getId());
		} catch (SQLException e)
		{
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
	}

	@Override
	public void delete(Notification d) throws MapperException {
		try {
			int count = NotificationTDG.delete(d.getId(), (int) d.getVersion());
			if(count==0) throw new LostUpdateException("Lost Update editing Notification with id " + d.getId());
		} catch (SQLException e)
		{
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
	}

	@Override
	public void cascadeInsert(Notification d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeUpdate(Notification d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeDelete(Notification d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateInsert(Notification d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateUpdate(Notification d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateDelete(Notification d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

}
