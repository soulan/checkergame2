package org.soen387.domain.model.notification.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.soen387.domain.model.notification.INotification;
import org.soen387.domain.model.notification.Notification;
import org.soen387.domain.model.notification.NotificationFactory;
import org.soen387.domain.model.notification.NotificationStatus;
import org.soen387.domain.model.notification.tdg.NotificationFinder;
import org.soen387.domain.model.player.IPlayer;
import org.soen387.domain.model.player.PlayerProxy;


public class NotificationInputMapper {
	public static ThreadLocal<Map<Long, Notification>> IM = new ThreadLocal<Map<Long,Notification>>() {
		protected java.util.Map<Long,Notification> initialValue() {
			return new HashMap<Long, Notification>();
		};
	};
	
	public static Notification find(long id) throws SQLException, DomainObjectNotFoundException {
		Notification p = IM.get().get(id);
		if(p!=null) return p;
		
		ResultSet rs = NotificationFinder.find(id);
		if(rs.next()) {
			p = buildNotification(rs);
			rs.close();
			IM.get().put(id, p);
			return p;
		}
		throw new DomainObjectNotFoundException("Could not create a Notification with id \""+id+"\"");	
	}
	
	public static Notification find(IPlayer u, NotificationStatus ns) throws SQLException, DomainObjectNotFoundException {
		ResultSet rs = NotificationFinder.findByPlayer(u.getUser().getId(), ns.getId());
		if(rs.next()) {
			long id = rs.getLong("id");
			Notification p = IM.get().get(id);
			if(p!=null) return p;
			p = buildNotification(rs);
			rs.close();
			IM.get().put(id, p);
			return p;
		}
		throw new DomainObjectNotFoundException("Could not create a Player from User with id \""+u.getId()+"\"");
	}
	
	public static List<INotification> find(IPlayer u) throws MapperException {
		try {
			ResultSet rs = NotificationFinder.findByPlayer(u.getId());
			return buildCollection(rs);
		} catch (SQLException e) {
            throw new MapperException(e);
        }
	}

	public static List<INotification> buildCollection(ResultSet rs)
		    throws SQLException {
		ArrayList<INotification> l = new ArrayList<INotification>();
	    while(rs.next()) {
	    	long id = rs.getLong("id");
	    	Notification c = IM.get().get(id);
	    	if(c == null) {
	    		c = buildNotification(rs);
	    		IM.get().put(id, c);
	    	}
	    	l.add(c);
	    }
	    return l;
	}

	public static List<INotification> findAll() throws MapperException {
        try {
            ResultSet rs = NotificationFinder.findAll();
            return buildCollection(rs);
        } catch (SQLException e) {
            throw new MapperException(e);
        }
	}
	
	private static Notification buildNotification(ResultSet rs) throws SQLException  {
		Notification p = NotificationFactory.createClean(rs.getLong("id"),
				rs.getInt("version"),
				NotificationStatus.values()[rs.getInt("status")],
				rs.getLong("type"),
				new PlayerProxy(rs.getLong("recipient")),
				rs.getBoolean("seen"));
		
		return p;
	}
}
