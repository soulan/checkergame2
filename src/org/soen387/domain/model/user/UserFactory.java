package org.soen387.domain.model.user;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.user.tdg.UserFinder;

public class UserFactory {
	public static User createNew (long id, String username, String password) throws SQLException, MapperException{
		User u = new User (id, 1, username, password);
		UoW.getCurrent().registerNew(u);
		return u;
	}
	
	public static User createNew (String username, String password) throws SQLException, MapperException{
		User u = new User (UserFinder.getMaxId(), 1, username, password);
		UoW.getCurrent().registerNew(u);
		return u;
	}
	
	public static User createClean (long id, int version, String username, String password) {
		User u = new User (id, version, username, password);
		UoW.getCurrent().registerClean(u);
		return u;
	}
}
