package org.soen387.domain.model.user.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.soen387.domain.model.user.User;
import org.soen387.domain.model.user.UserFactory;
import org.soen387.domain.model.user.tdg.UserFinder;

public class UserInputMapper {
	public static ThreadLocal<Map<Long, User>> IM = new ThreadLocal<Map<Long,User>>() {
		protected java.util.Map<Long,User> initialValue() {
			return new HashMap<Long, User>();
		};
	};
	
	public static User find(long id) throws SQLException, DomainObjectNotFoundException {
		User u = IM.get().get(id);
		if(u!=null) return u;
		
		ResultSet rs = UserFinder.find(id);
		if(rs.next()) {
			u = buildUser(rs);
			rs.close();
			IM.get().put(id, u);
			return u;
		}
		throw new DomainObjectNotFoundException("Could not create a user with id \""+id+"\"");
	}

	public static User find(String username, String password) throws SQLException, DomainObjectNotFoundException {
		User u = null;
		
		ResultSet rs = UserFinder.find(username, password);
		if(rs.next()) {
			Long id = rs.getLong("id");
			u = IM.get().get(id);
			if(u!=null) {
				rs.close();
				return u;
			}
			
			u = buildUser(rs);
			rs.close();
			IM.get().put(id, u);
			return u;
		}
		throw new DomainObjectNotFoundException("Could not create a user with that username and password combination.");
	}	
	

	private static User buildUser(ResultSet rs) throws SQLException  {
		User u = UserFactory.createClean(rs.getLong("id"), rs.getInt("version"), rs.getString("username"), "");
		return u; //we don't actually dig out the password, that'd be cray-cray.
	}
}
