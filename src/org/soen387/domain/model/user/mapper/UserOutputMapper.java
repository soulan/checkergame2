package org.soen387.domain.model.user.mapper;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.mapper.IOutputMapper;
import org.dsrg.soenea.domain.mapper.LostUpdateException;
import org.soen387.domain.model.user.User;
import org.soen387.domain.model.user.tdg.UserTDG;

public class UserOutputMapper implements IOutputMapper<Long, User>{
	@Override
	public void insert(User u) throws MapperException {
		try {
			UserTDG.insert(u.getId(), (int) u.getVersion(), u.getUsername(), u.getPassword());
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
		u.setVersion(1);
	}

	@Override
	public void update(User u) throws MapperException {
		try {
			int count = UserTDG.update(u.getId(), (int) u.getVersion(), u.getUsername(), u.getPassword());
			if(count==0) throw new LostUpdateException("Lost Update editing user with id " + u.getId());
			u.setVersion(u.getVersion()+1);
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}		
	}

	@Override
	public void delete(User u) throws MapperException {
		try {
			int count = UserTDG.delete(u.getId(), (int) u.getVersion());
			if(count==0) throw new LostUpdateException("Lost Update deleting user with id " + u.getId());
			//
			// What's the process for deleting a User... do we need to delete players and games?
			// More on that when we discuss referential integrity.
			//
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}		
	}

	@Override
	public void cascadeInsert(User d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeUpdate(User d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeDelete(User d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateInsert(User d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateUpdate(User d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateDelete(User d) throws MapperException {
		// TODO Auto-generated method stub
		
	}
}
