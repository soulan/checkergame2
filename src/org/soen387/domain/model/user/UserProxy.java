package org.soen387.domain.model.user;

import java.sql.SQLException;

import org.dsrg.soenea.domain.DomainObjectCreationException;
import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.proxy.DomainObjectProxy;
import org.soen387.domain.model.user.mapper.UserInputMapper;

public class UserProxy extends DomainObjectProxy<Long, User> implements IUser {
	long id;
	
	public UserProxy(long id) {
		super(id);
		this.id = id;
	}

	public User getInner() {
		try {
			return UserInputMapper.find(id);
		} catch (Exception e) {
			// It better be here! That null won't go over well!
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public long getVersion() {
		return getInner().getVersion();
	}

	@Override
	public void setVersion(int version) {
		getInner().setVersion(version);
	}

	@Override
	public String getUsername() {
		return getInner().getUsername();
	}

	@Override
	public void setUsername(String username) {
		getInner().setUsername(username);
	}

	@Override
	public String getPassword() {
		return getInner().getPassword();
	}

	@Override
	public void setPassword(String password) {
		getInner().setPassword(password);
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	protected User getFromMapper(Long id) throws MapperException,
			DomainObjectCreationException {
		User u = null;
		try {
			u = UserInputMapper.find(id);
		} catch (SQLException e) {
			throw new MapperException (e.getMessage());
		}
		return u;
	}

}
