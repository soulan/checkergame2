package org.soen387.domain.model.player.mapper;

import java.sql.SQLException;
import java.util.List;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.mapper.IOutputMapper;
import org.dsrg.soenea.domain.mapper.LostUpdateException;
import org.dsrg.soenea.domain.role.IRole;
import org.soen387.domain.model.player.Player;
import org.soen387.domain.model.player.tdg.PlayerTDG;

public class PlayerOutputMapper implements IOutputMapper<Long, Player> {
	@Override
	public void insert(Player p) throws MapperException {
		//List<IRole> roles = p.getRoles();
		//System.out.println("----role:" + roles.size());
			
		long role = 2;
		
		try{
			PlayerTDG.insert(p.getId(), (int) p.getVersion(), p.getFirstName(), p.getLastName(), p.getEmail(), p.getUser().getId(), role);
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
		p.setVersion(1);
	}

	@Override
	public void update(Player p) throws MapperException {
		List<IRole> roles = p.getRoles();
		long role = roles.get(0).getId();
		
		try{
			int count = PlayerTDG.update(p.getId(), (int) p.getVersion(), p.getFirstName(), p.getLastName(), p.getEmail(), p.getUser().getId(), role);
			if(count==0) throw new LostUpdateException("Lost Update editing player with id " + p.getId());
			p.setVersion((int) p.getVersion()+1);
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
	}

	@Override
	public void delete(Player p) throws MapperException {
		try {
			int count = PlayerTDG.delete(p.getId(), (int) p.getVersion());
			if(count==0) throw new LostUpdateException("Lost Update deleting player with id " + p.getId());
			//
			// What's the process for deleting a Player... do we need to delete users and games?
			// More on that when we discuss referential integrity.
			//
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}		
	}

	@Override
	public void cascadeInsert(Player d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeUpdate(Player d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeDelete(Player d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateInsert(Player d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateUpdate(Player d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateDelete(Player d) throws MapperException {
		// TODO Auto-generated method stub
		
	}
}
