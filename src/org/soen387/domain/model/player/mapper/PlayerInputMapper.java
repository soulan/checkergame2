package org.soen387.domain.model.player.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.dsrg.soenea.domain.role.IRole;
import org.soen387.domain.model.player.IPlayer;
import org.soen387.domain.model.player.Player;
import org.soen387.domain.model.player.PlayerFactory;
import org.soen387.domain.model.player.tdg.PlayerFinder;
import org.soen387.domain.model.role.PlayerRole;
import org.soen387.domain.model.user.User;
import org.soen387.domain.model.user.UserProxy;

public class PlayerInputMapper {
	public static ThreadLocal<Map<Long, Player>> IM = new ThreadLocal<Map<Long,Player>>() {
		protected java.util.Map<Long,Player> initialValue() {
			return new HashMap<Long, Player>();
		};
	};
	
	public static Player find(long id) throws SQLException, DomainObjectNotFoundException {
		Player p = IM.get().get(id);
		if(p!=null) return p;
		
		ResultSet rs = PlayerFinder.find(id);
		if(rs.next()) {
			p = buildPlayer(rs);
			rs.close();
			IM.get().put(id, p);
			return p;
		}
		throw new DomainObjectNotFoundException("Could not create a Player with id \""+id+"\"");
	}
	
	public static Player find(User u) throws SQLException, DomainObjectNotFoundException {
		ResultSet rs = PlayerFinder.findByUser(u.getId());
		if(rs.next()) {
			long id = rs.getLong("id");
			Player p = IM.get().get(id);
			if(p!=null) return p;
			p = buildPlayer(rs);
			rs.close();
			IM.get().put(id, p);
			return p;
		}
		throw new DomainObjectNotFoundException("Could not create a Player from User with id \""+u.getId()+"\"");
	}

	public static List<IPlayer> buildCollection(ResultSet rs)
		    throws SQLException {
		    ArrayList<IPlayer> l = new ArrayList<IPlayer>();
		    List<IRole> role=null;
		    if(rs.next()){
		    		int test = rs.getInt("role");	
		    	if (test == 2){
		    		role = new ArrayList<IRole>();
		    		PlayerRole pr = new PlayerRole();
		    		role.add(pr);
		    	}
		    }
		    while(rs.next()) {
		    	Player p = PlayerFactory.createClean(rs.getLong("id"), rs.getInt("version"), rs.getString("firstname"), rs.getString("lastname"), 
		    			rs.getString("email"), new UserProxy(rs.getLong("user")),role);
		    	Player c = IM.get().get(p.getId());
		    	if(c == null) {
		    		c = buildPlayer(rs);
		    		IM.get().put(p.getId(), c);
		    	}
		    	l.add(c);
		    }
		    return l;
		}

	public static List<IPlayer> findAll() throws MapperException {
        try {
            ResultSet rs = PlayerFinder.findAll();
            return buildCollection(rs);
        } catch (SQLException e) {
            throw new MapperException(e);
        }
	}
	
	private static Player buildPlayer(ResultSet rs) throws SQLException  {
		// TODO Auto-generated method stub
		int test = rs.getInt("role");
		List<IRole> role= null;
		if (test == 2){
			role = new ArrayList<IRole>();
			PlayerRole pr = new PlayerRole();
			role.add(pr);
		}
		
		Player p = PlayerFactory.createClean(rs.getLong("id"),
				rs.getInt("version"),
				rs.getString("firstname"),
				rs.getString("lastname"),
				rs.getString("email"),
				new UserProxy(rs.getLong("user")),
				role);
		
		return p;
	}
}
