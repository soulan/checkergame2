package org.soen387.domain.model.player;

import java.util.List;

import org.dsrg.soenea.domain.role.IRole;
import org.dsrg.soenea.domain.user.User;
import org.soen387.domain.model.user.IUser;

public class Player extends User implements IPlayer {
	long id;
	int version;
	String firstName;
	String lastName;
	String email;
	IUser user;
	List<IRole> roles;

	@Override
	public long getVersion() {
		return version;
	}

	@Override
	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public IUser getUser() {
		return user;
	}

	@Override
	public void setUser(IUser user) {
		this.user = user;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	public List<IRole> getRoles() {
		return roles;
	}

	public void setRoles(List<IRole> roles) {
		this.roles = roles;
		
	}

	public boolean hasRole(Class<? extends IRole> role) {
		for(IRole r: roles) {
			if(role.isInstance(r)) return true;
		}
		return false;
	}

	public Player(long id, int version, String firstName, String lastName,
			String email, IUser user, List<IRole> roles) {
		super(id, (int) version, user.getUsername(), roles);
		this.id = id;
		this.version = version;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.user = user;
		this.roles = roles;
	}

	@Override
	public boolean equals(Object p) {
		return p instanceof IPlayer && this.id==((IPlayer)(p)).getId();
	}

}
