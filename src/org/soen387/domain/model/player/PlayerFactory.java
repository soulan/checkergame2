package org.soen387.domain.model.player;

import java.sql.SQLException;
import java.util.List;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.role.IRole;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.player.tdg.PlayerFinder;
import org.soen387.domain.model.user.IUser;

public class PlayerFactory {
	public static Player createNew(String firstName, String lastName, String email, IUser u, List<IRole> roles) throws SQLException, MapperException{
		return createNew(PlayerFinder.getMaxId(), firstName, lastName, email, u, roles);
	}
	
	public static Player createNew (long id, String firstName, String lastName, String email, IUser user, List<IRole> roles) throws SQLException, MapperException {
		Player p = new Player (id, 1, firstName, lastName, email, user, roles);
		UoW.getCurrent().registerNew(p); 
		return p;
	}
	
	public static Player createClean (long id, int version, String firstName, String lastName, String email, IUser user, List<IRole> roles) {
		Player p = new Player (id, version, firstName, lastName, email, user, roles);
		UoW.getCurrent().registerClean(p);
		return p;
	}
}
