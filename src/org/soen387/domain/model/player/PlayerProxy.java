package org.soen387.domain.model.player;

import java.sql.SQLException;
import java.util.List;

import org.dsrg.soenea.domain.DomainObjectCreationException;
import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.proxy.DomainObjectProxy;
import org.dsrg.soenea.domain.role.IRole;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;
import org.soen387.domain.model.user.IUser;

public class PlayerProxy extends DomainObjectProxy<Long, Player> implements IPlayer {
	long id;
	
	public PlayerProxy(long id) {
		super(id);
		this.id = id;
	}

	public Player getInner() {
		try {
			return PlayerInputMapper.find(id);
		} catch (Exception e) {
			// It better be here! That null won't go over well!
			e.printStackTrace();
			return null;
		}
	}	
	
	@Override
	public long getVersion() {
		return getInner().getVersion();
	}

	@Override
	public void setVersion(int version) {
		getInner().setVersion(version);
	}

	@Override
	public String getFirstName() {
		return getInner().getFirstName();
	}

	@Override
	public void setFirstName(String firstName) {
		getInner().setFirstName(firstName);
	}

	@Override
	public String getLastName() {
		return getInner().getLastName();
	}

	@Override
	public void setLastName(String lastName) {
		getInner().setLastName(lastName);
	}

	@Override
	public String getEmail() {
		return getInner().getEmail();
	}

	@Override
	public void setEmail(String email) {
		getInner().setEmail(email);
	}

	@Override
	public IUser getUser() {
		return getInner().getUser();
	}

	@Override
	public void setUser(IUser user) {
		getInner().setUser(user);
	}

	@Override
	public boolean equals(Object p) {
		return p instanceof IPlayer && this.id==((IPlayer)(p)).getId();
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}	

	@Override
	public List<IRole> getRoles() {
		// TODO Auto-generated method stub
		return getInnerObject().getRoles();
	}
	
	@Override
	public void setRoles(List<IRole> roles) {
		// TODO Auto-generated method stub
		getInnerObject().setRoles(roles);
	}
	
	@Override
	public boolean hasRole(Class<? extends IRole> role) {
		// TODO Auto-generated method stub
		return getInnerObject().hasRole(role);
	}

	@Override
	protected Player getFromMapper(Long id) throws MapperException,
			DomainObjectCreationException {
		Player p = null;
		try{
			p = PlayerInputMapper.find(id);			
		} catch (SQLException e){
			throw new MapperException(e.getMessage());
		}
		return p;
	}
	
}
