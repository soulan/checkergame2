package org.soen387.domain.model.challenge;

import java.sql.SQLException;

import org.dsrg.soenea.domain.DomainObjectCreationException;
import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.proxy.DomainObjectProxy;
import org.soen387.domain.model.challenge.mapper.ChallengeInputMapper;
import org.soen387.domain.model.player.IPlayer;

public class ChallengeProxy extends DomainObjectProxy<Long, Challenge> implements IChallenge{
	long id;
	
	public ChallengeProxy(long id) {
		super(id);
		this.id = id;
	}

	public Challenge getInner() {
		try {
			return ChallengeInputMapper.find(id);
		} catch (Exception e) {
			// It better be here! That null won't go over well!
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public long getVersion() {
		return getInner().getVersion();
	}

	@Override
	public void setVersion(int version) {
		getInner().setVersion(version);
	}


	@Override
	public Long getId() {
		return id;
	}

	@Override
	public IPlayer getChallenger() {
		return getInner().getChallenger();
	}

	@Override
	public void setChallenger(IPlayer challenger) {
		getInner().setChallenger(challenger);
		
	}

	@Override
	public IPlayer getChallengee() {
		return getInner().getChallengee();
	}

	@Override
	public void setChallengee(IPlayer challengee) {
		getInner().setChallengee(challengee);
		
	}

	@Override
	public ChallengeStatus getStatus() {
		return getInner().getStatus();
	}

	@Override
	public void setStatus(ChallengeStatus status) {
		getInner().setStatus(status);
	}

	@Override
	protected Challenge getFromMapper(Long id) throws MapperException,
			DomainObjectCreationException {
		Challenge c = null;
		try {
			c = ChallengeInputMapper.find(id);
		} catch (SQLException e){
			throw new MapperException(e.getMessage());
		}
		return c;
	}

}
