package org.soen387.domain.model.challenge.mapper;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.mapper.IOutputMapper;
import org.dsrg.soenea.domain.mapper.LostUpdateException;
import org.soen387.domain.model.challenge.Challenge;
import org.soen387.domain.model.challenge.tdg.ChallengeTDG;

public class ChallengeOutputMapper implements IOutputMapper<Long, Challenge>{
	@Override
	public void insert(Challenge c) throws MapperException {
		try {
			ChallengeTDG.insert(c.getId(), (int) c.getVersion(), c.getChallenger().getId(), c.getChallengee().getId(), c.getStatus().getId());
		} catch (SQLException e)
		{
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
		
	}

	@Override
	public void update(Challenge c) throws MapperException {
		try {
			int count = ChallengeTDG.update(c.getId(), (int) c.getVersion(), c.getChallenger().getUser().getId(), c.getChallengee().getUser().getId(), c.getStatus().getId());
			if(count==0) throw new LostUpdateException("Lost Update editing Challenge with id " + c.getId());
		} catch (SQLException e)
		{
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
	}

	@Override
	public void delete(Challenge c) throws MapperException {
		try{
			int count = ChallengeTDG.delete(c.getId(), (int) c.getVersion());
			if(count==0) throw new LostUpdateException("Lost Update deleting Challenge with id " + c.getId());
		} catch (SQLException e)
		{
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
	}

	@Override
	public void cascadeInsert(Challenge d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeUpdate(Challenge d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeDelete(Challenge d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateInsert(Challenge d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateUpdate(Challenge d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateDelete(Challenge d) throws MapperException {
		// TODO Auto-generated method stub
		
	}
}
