package org.soen387.domain.model.checkerboard;

import java.awt.Point;
import java.sql.SQLException;

import org.dsrg.soenea.domain.DomainObjectCreationException;
import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.proxy.DomainObjectProxy;
import org.soen387.domain.model.checkerboard.mapper.CheckerBoardInputMapper;
import org.soen387.domain.model.player.IPlayer;


public class CheckerBoardProxy extends DomainObjectProxy<Long, CheckerBoard> implements ICheckerBoard {
	long id;
	
	public CheckerBoardProxy(long id) {
		super(id);
		this.id = id;
	}

	public CheckerBoard getInner() {
		try {
			return CheckerBoardInputMapper.find(id);
		} catch (Exception e) {
			// It better be here! That null won't go over well!
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	public long getVersion() {
		return getInner().getVersion();
	}

	@Override
	public void setVersion(int version) {
		getInner().setVersion(version);
	}


	@Override
	public Long getId() {
		return id;
	}

	@Override
	public GameStatus getStatus() {
		return getInner().getStatus();
	}

	@Override
	public void setStatus(GameStatus status) {
		getInner().setStatus(status);
	}

	@Override
	public String getPieces() {
		return getInner().getPieces();
	}

	@Override
	public void setPieces(String pieces) {
		getInner().setPieces(pieces);
	}

	@Override
	public IPlayer getFirstPlayer() {
		return getInner().getFirstPlayer();
	}

	@Override
	public void setFirstPlayer(IPlayer firstPlayer) {
		getInner().setFirstPlayer(firstPlayer);
	}

	@Override
	public IPlayer getSecondPlayer() {
		return getInner().getSecondPlayer();
	}

	@Override
	public void setSecondPlayer(IPlayer secondPlayer) {
		getInner().setSecondPlayer(secondPlayer);
	}

	@Override
	public IPlayer getCurrentPlayer() {
		return getInner().getCurrentPlayer();
	}

	@Override
	public void setCurrentPlayer(IPlayer currentPlayer) {
		getInner().setCurrentPlayer(currentPlayer);
	}

	@Override
	public void move(Point source, Point target) {
		getInner().move(source, target);
	}

	@Override
	public void jump(Point source, Point... targets) {
		getInner().jump(source, targets);
	}

	@Override
	protected CheckerBoard getFromMapper(Long id) throws MapperException,
			DomainObjectCreationException {
		try{
			return CheckerBoardInputMapper.find(id);
		} catch (SQLException e){
			throw new MapperException(e.getMessage());
		}
	}

}
