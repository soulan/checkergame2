package org.soen387.domain.model.checkerboard.mapper;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.mapper.IOutputMapper;
import org.dsrg.soenea.domain.mapper.LostUpdateException;
import org.soen387.domain.model.checkerboard.CheckerBoard;
import org.soen387.domain.model.checkerboard.tdg.CheckerBoardTDG;

public class CheckerBoardOutputMapper implements IOutputMapper<Long, CheckerBoard> {
	@Override
	public void insert(CheckerBoard cb) throws MapperException {
		try {
			CheckerBoardTDG.insert(cb.getId(), (int) cb.getVersion(), cb.getStatus().getId(), cb.getPieces(), cb.getFirstPlayer().getUser().getId(), cb.getSecondPlayer().getUser().getId(), cb.getCurrentPlayer().getUser().getId());			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
	}

	@Override
	public void update(CheckerBoard cb) throws MapperException {
		try {
			int count = CheckerBoardTDG.update(cb.getId(), (int) cb.getVersion(), cb.getStatus().getId(), cb.getPieces(), cb.getFirstPlayer().getUser().getId(), cb.getSecondPlayer().getUser().getId(), cb.getCurrentPlayer().getUser().getId());
			if(count==0) throw new LostUpdateException("Lost Update editing CheckerBoard with id " + cb.getId());
			cb.setVersion(cb.getVersion()+1);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
	}

	@Override
	public void delete(CheckerBoard cb) throws MapperException {
		try {
			int count = CheckerBoardTDG.delete(cb.getId(), (int) cb.getVersion());
			if(count==0) throw new LostUpdateException("Lost Update deleting CheckerBoard with id " + cb.getId());
			//
			// What's the process for deleting a CheckerBoard... do we need to delete them?
			// More on that when we discuss referential integrity.
			//
		} catch (SQLException e) {
			e.printStackTrace();
			throw new MapperException(e.getMessage());
		}
	}

	@Override
	public void cascadeInsert(CheckerBoard d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeUpdate(CheckerBoard d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cascadeDelete(CheckerBoard d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateInsert(CheckerBoard d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateUpdate(CheckerBoard d) throws MapperException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateDelete(CheckerBoard d) throws MapperException {
		// TODO Auto-generated method stub
		
	}	
}
