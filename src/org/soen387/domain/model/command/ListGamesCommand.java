package org.soen387.domain.model.command;

import java.util.List;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.command.impl.Command;
import org.dsrg.soenea.domain.helper.Helper;
import org.soen387.domain.model.checkerboard.ICheckerBoard;
import org.soen387.domain.model.checkerboard.mapper.CheckerBoardInputMapper;

public class ListGamesCommand extends Command {

	public ListGamesCommand(Helper helper) {
		super(helper);
	}

	public void process() throws CommandException {
		try {
			
			List<ICheckerBoard> Games;
			Games=CheckerBoardInputMapper.findAll();
			helper.setRequestAttribute("games", Games);
		} catch (MapperException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setUp() throws CommandException {
	
	}

}
