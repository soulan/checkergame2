package org.soen387.domain.model.command;

import java.sql.SQLException;

import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.soen387.domain.model.checkerboard.GameStatus;
import org.soen387.domain.model.checkerboard.ICheckerBoard;
import org.soen387.domain.model.checkerboard.mapper.CheckerBoardInputMapper;
import org.soen387.domain.model.command.exception.CanOnlyMoveOnYourTurnException;
import org.soen387.domain.model.command.exception.CanOnlyRespondToOngoingGameException;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;
import org.soen387.domain.model.player.IPlayer;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;

public class MakeGameMoveCommand extends CheckersCommand{
	public ICheckerBoard checkerBoard;
	public String pieces;

	public MakeGameMoveCommand(Helper helper) {
		super(helper);
		try {
			currentPlayer = PlayerInputMapper.find((long) helper.getSessionAttribute("playerid"));
			checkerBoard = CheckerBoardInputMapper.find((long)helper.getAttribute("boardid"));
			pieces = (String)helper.getAttribute("pieces");
		} catch (DomainObjectNotFoundException | SQLException e) {
			System.out.println("COULD NOT FIND PLAYER ID / CHECKERBOARD ID");
			e.printStackTrace();
		}
	}

	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws CommandException {
		// player is logged in
		if(currentPlayer==null) {
			throw new NeedToBeLoggedInException();
		}
		
		// game is ongoing
		if(!checkerBoard.getStatus().equals(GameStatus.Ongoing)){
			throw new CanOnlyRespondToOngoingGameException();
		}
		
		// player is involved in game
		if(checkerBoard.getCurrentPlayer().getId() == currentPlayer.getId()){
			throw new CanOnlyMoveOnYourTurnException();
		}
		
		// other player becomes currentPlayer
		checkerBoard.setPieces(pieces);
		IPlayer next;
		if (checkerBoard.getFirstPlayer().getUser().getId().equals(currentPlayer.getUser().getId())){
			next = checkerBoard.getSecondPlayer();			
		}
		else
			next = checkerBoard.getFirstPlayer();
		checkerBoard.setCurrentPlayer(next);
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
