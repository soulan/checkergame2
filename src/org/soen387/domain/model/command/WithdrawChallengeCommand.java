package org.soen387.domain.model.command;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.challenge.ChallengeStatus;
import org.soen387.domain.model.challenge.IChallenge;
import org.soen387.domain.model.challenge.mapper.ChallengeInputMapper;
import org.soen387.domain.model.command.exception.CanOnlyRespondToChallengeIssuedByYouException;
import org.soen387.domain.model.command.exception.CanOnlyRespondToOpenChallengesException;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;
import org.soen387.domain.model.notification.Notification;
import org.soen387.domain.model.notification.NotificationStatus;
import org.soen387.domain.model.notification.mapper.NotificationInputMapper;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;

public class WithdrawChallengeCommand extends CheckersCommand {
	public IChallenge challenge;
	public long version;
	public ChallengeStatus status;	

	public WithdrawChallengeCommand(Helper helper) {
		super(helper);
		try {
			currentPlayer = PlayerInputMapper.find((long) helper.getSessionAttribute("playerid"));	
			challenge = ChallengeInputMapper.find((long)helper.getAttribute("challengeid"));
			status = ChallengeStatus.Refused;
			version = (long)helper.getAttribute("challengeversion");
		} catch (DomainObjectNotFoundException | SQLException e) {
			System.out.println("COULD NOT FIND PLAYER ID / CHALLENGE ID / STATUS ID");
			e.printStackTrace();
		}
	}

	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws CommandException {
		try {
			// Check to see if there is any current player; challengee cannot be current player; Challenge Status is not open
			if(currentPlayer==null) {
				throw new NeedToBeLoggedInException();
			}
			
			challenge.setVersion(version);
			
			if(!challenge.getChallenger().equals(currentPlayer)) {
				throw new CanOnlyRespondToChallengeIssuedByYouException();
			}
			
			if(!challenge.getStatus().equals(ChallengeStatus.Open)) {
				throw new CanOnlyRespondToOpenChallengesException();
			}
			
			// Change the status from Open to Refused
			challenge.setStatus(status);
			
			if(status.equals(ChallengeStatus.Refused)) {	
				Notification n = NotificationInputMapper.find(challenge.getChallengee(), NotificationStatus.Issued);
				UoW.getCurrent().registerRemoved(n);
			}
			
			UoW.getCurrent().registerDirty(challenge);	
		} catch (MapperException | SQLException e) {
			throw new CommandException(e);
		}
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
