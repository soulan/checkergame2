package org.soen387.domain.model.command;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.soen387.domain.model.command.exception.NoSuchUserException;
import org.soen387.domain.model.command.CheckersCommand;
import org.soen387.domain.model.player.IPlayer;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;
import org.soen387.domain.model.user.User;
import org.soen387.domain.model.user.mapper.UserInputMapper;

public class LogInCommand  extends CheckersCommand {
	public String username;
	public String password;
	public IPlayer player;

	public LogInCommand(Helper helper) {
		super(helper);
		username = (String)helper.getAttribute("username");
		password = (String) helper.getAttribute("password");
	}

	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws CommandException {
		try {
			User u = null;
			try {
				u = UserInputMapper.find(username, password);
			} catch (final DomainObjectNotFoundException e) {
				throw new NoSuchUserException();
			}
			player = currentPlayer = PlayerInputMapper.find(u);
		} catch (MapperException | SQLException e) {
			// Something bad has happened
			throw new CommandException(e);
		}

		// Security Check!
		helper.invalidateSession();

		// Store PlayerId in session
		helper.setRequestAttribute("player", player);
		helper.setSessionAttribute("playerid" , currentPlayer.getId());
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	} 

}
