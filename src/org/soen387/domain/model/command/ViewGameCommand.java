package org.soen387.domain.model.command;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.command.impl.Command;
import org.dsrg.soenea.domain.helper.Helper;
import org.soen387.domain.model.checkerboard.ICheckerBoard;
import org.soen387.domain.model.checkerboard.mapper.CheckerBoardInputMapper;

public class ViewGameCommand extends Command{
	public ICheckerBoard checkerboard;
	public long id;
	public String idAsString;
	
	public ViewGameCommand(Helper helper) {
		super(helper);
	}

	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws CommandException {		
		try {
			id=(long)helper.getAttribute("boardid");
			checkerboard = CheckerBoardInputMapper.find(id);
			helper.setRequestAttribute("checkerboard", checkerboard);
		} catch (MapperException | SQLException e) {
			throw new CommandException(e);
		}
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
