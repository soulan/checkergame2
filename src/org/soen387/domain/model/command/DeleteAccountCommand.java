package org.soen387.domain.model.command;

import java.sql.SQLException;
import java.util.List;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.interf.IDomainObject;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.dsrg.soenea.uow.MissingMappingException;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.challenge.ChallengeStatus;
import org.soen387.domain.model.challenge.IChallenge;
import org.soen387.domain.model.challenge.mapper.ChallengeInputMapper;
import org.soen387.domain.model.checkerboard.GameStatus;
import org.soen387.domain.model.checkerboard.ICheckerBoard;
import org.soen387.domain.model.checkerboard.mapper.CheckerBoardInputMapper;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;
import org.soen387.domain.model.notification.Notification;
import org.soen387.domain.model.notification.NotificationFactory;
import org.soen387.domain.model.notification.NotificationStatus;
import org.soen387.domain.model.notification.mapper.NotificationInputMapper;
import org.soen387.domain.model.player.IPlayer;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;

public class DeleteAccountCommand extends CheckersCommand {
	long id;
	String domainName = "checkers.ca";

	public DeleteAccountCommand(Helper helper) {
		super(helper);
		try {
			currentPlayer = PlayerInputMapper.find((long) helper.getSessionAttribute("playerid"));
			id = (long)helper.getAttribute("id");
		} catch (DomainObjectNotFoundException | SQLException e) {
			System.out.println("COULD NOT FIND PLAYER ID");
			e.printStackTrace();
		}
	}

	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void process() throws CommandException {
		// player is logged in
		if(currentPlayer==null) {
			throw new NeedToBeLoggedInException();
		}
		
		try {
			// Withdraw challenges
			List<IChallenge> challenge = ChallengeInputMapper.find(currentPlayer);
			for(IChallenge c: challenge){
				if(c.getStatus().getId() == 0){
					c.setStatus(ChallengeStatus.Refused);
					Notification n = NotificationInputMapper.find(c.getChallengee(), NotificationStatus.Issued);
					UoW.getCurrent().registerRemoved(n);
					UoW.getCurrent().registerDirty(c);
				}
			}		
			
			List<ICheckerBoard> checkerboard = CheckerBoardInputMapper.find(currentPlayer);
			for(ICheckerBoard cb: checkerboard){
				if(cb.getStatus().getId() == 0){
					cb.setStatus(GameStatus.Over);
					IPlayer recipient;
					if (cb.getFirstPlayer().getUser().getId().equals(currentPlayer.getUser().getId())){
						recipient = cb.getSecondPlayer();			
					}
					else
						recipient = cb.getFirstPlayer();
					NotificationFactory.createNew(NotificationStatus.Conceded, cb, recipient);
					NotificationFactory.createNew(NotificationStatus.Won,cb, recipient);
				}
			}
			
			// On deletion, passwords should be set to "-" or some unhashable value for the password system
			currentPlayer.getUser().setPassword("-");;
			
			// On deletion fields should be replaced with del_[ID] where [ID] is the id of that Domain Object
			currentPlayer.getUser().setUsername("del_" + currentPlayer.getUser().getId());
			currentPlayer.setFirstName("del_" + currentPlayer.getUser().getId());
			currentPlayer.setLastName("del_" + currentPlayer.getUser().getId());
			
			// email should be set to del_[ID]@[DOMAIN] where [ID] is the id of that Domain Object, and [DOMAIN] is the domain name of the checkers site (yes, this is fictitious)
			currentPlayer.setEmail("del_" + currentPlayer.getUser().getId() + "@" + domainName);
			
			UoW.getCurrent().registerDirty(currentPlayer.getUser());
			UoW.getCurrent().registerDirty((IDomainObject<IPlayer>) currentPlayer);
			
		} catch (MissingMappingException | MapperException | SQLException e){
			System.out.println("COULD NOT DELETE USER");
			e.printStackTrace();
		}
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
