package org.soen387.domain.model.command;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.dsrg.soenea.uow.MissingMappingException;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.checkerboard.GameStatus;
import org.soen387.domain.model.checkerboard.ICheckerBoard;
import org.soen387.domain.model.checkerboard.mapper.CheckerBoardInputMapper;
import org.soen387.domain.model.command.exception.CanOnlyConcedeGameIfPlayerIsInvolvedException;
import org.soen387.domain.model.command.exception.CanOnlyRespondToOngoingGameException;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;
import org.soen387.domain.model.notification.NotificationFactory;
import org.soen387.domain.model.notification.NotificationStatus;
import org.soen387.domain.model.player.IPlayer;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;

public class ConcedeGameCommand extends CheckersCommand {
	public ICheckerBoard checkerBoard;
	public GameStatus status;
	public long version;
	
	public ConcedeGameCommand(Helper helper) {
		super(helper);
		try {
			currentPlayer = PlayerInputMapper.find((long) helper.getSessionAttribute("playerid"));
			checkerBoard = CheckerBoardInputMapper.find((long)helper.getAttribute("boardid"));
			status = GameStatus.Over;
		} catch (DomainObjectNotFoundException | SQLException e) {
			System.out.println("COULD NOT FIND PLAYER ID / CHECKERBOARD ID / STATUS ID");
			e.printStackTrace();
		}
	}

	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws CommandException {
		// player is logged in
		if(currentPlayer==null) {
			throw new NeedToBeLoggedInException();
		}
		
		// game is ongoing
		if(!checkerBoard.getStatus().equals(GameStatus.Ongoing)){
			throw new CanOnlyRespondToOngoingGameException();
		}
		
		// player is involved in game
		if(!checkerBoard.getFirstPlayer().getUser().getId().equals(currentPlayer.getUser().getId()) &&
				!checkerBoard.getSecondPlayer().getUser().getId().equals(currentPlayer.getUser().getId())){
			throw new CanOnlyConcedeGameIfPlayerIsInvolvedException();
		}

		// Change status from Ongoing to Over and notify
		checkerBoard.setStatus(status);
		IPlayer recipient;
		if (checkerBoard.getFirstPlayer().getUser().getId().equals(currentPlayer.getUser().getId())){
			recipient = checkerBoard.getSecondPlayer();			
		}
		else
			recipient = checkerBoard.getFirstPlayer();
		try {
			NotificationFactory.createNew(NotificationStatus.Conceded, checkerBoard, recipient);
			NotificationFactory.createNew(NotificationStatus.Won,checkerBoard, recipient);
		} catch (SQLException | MapperException e1) {
			e1.printStackTrace();
			System.out.println("COULD NOT NOTIFY!");
		}
				
		try {
			UoW.getCurrent().registerDirty(checkerBoard);
		} catch (MissingMappingException | MapperException e) {
			System.out.println("COULD NOT REGISTER DIRTY UOW WITH CONCEDE GAME");
			throw new CommandException(e);
		}
		helper.setRequestAttribute("checkerboard", checkerBoard);
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
