package org.soen387.domain.model.command;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.role.IRole;
import org.soen387.domain.model.player.PlayerFactory;
import org.soen387.domain.model.role.PlayerRole;
import org.soen387.domain.model.user.IUser;
import org.soen387.domain.model.user.UserFactory;

public class RegisterPlayerCommand extends CheckersCommand{
	public String username;
	public String password;
	public String firstName;
	public String lastName;
	public String email;
	public List<IRole> roles;

	public RegisterPlayerCommand(Helper helper) {
		super(helper);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws CommandException {
		IUser u;
		username = (String) helper.getAttribute("username");
		password = (String) helper.getAttribute("password");
		firstName = (String) helper.getAttribute("firstName");
		lastName = (String) helper.getAttribute("lastName");
		email = (String) helper.getAttribute("email");
		IRole playerRole = new PlayerRole();
		roles = new ArrayList<IRole>();
		roles.add(playerRole);
		
		try {			
			u = UserFactory.createNew(username, password); 
			PlayerFactory.createNew(firstName, lastName, email, u, roles); 
		} catch (SQLException | MapperException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println ("REGISTER PLAYER: COULD NOT ADD NEW PLAYER!!");
		}
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
