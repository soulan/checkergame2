package org.soen387.domain.model.command;

import java.util.List;

import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.soen387.domain.model.checkerboard.ICheckerBoard;
import org.soen387.domain.model.checkerboard.mapper.CheckerBoardInputMapper;
import org.soen387.domain.model.player.IPlayer;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;

public class ViewPlayerStatsCommand extends CheckersCommand {
	
	public long id;
	public IPlayer p;
	public List<ICheckerBoard> games;
	
	public ViewPlayerStatsCommand(Helper helper) {
		super(helper);
		id=(long)helper.getAttribute("id");
		// TODO Auto-generated constructor stub
	}
	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub	
	}
	@Override
	public void process() throws CommandException {
		try{
		p=PlayerInputMapper.find(this.id);
		games = CheckerBoardInputMapper.findByUserId(this.id);	
		helper.setRequestAttribute("games",games);
		helper.setRequestAttribute("player",p);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
