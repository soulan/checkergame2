package org.soen387.domain.model.command.exception;

import org.dsrg.soenea.domain.command.CommandException;

public class CanOnlyMoveOnYourTurnException extends CommandException {

	private static final long serialVersionUID = -1535311749307309832L;

	public CanOnlyMoveOnYourTurnException() {
		super();
	}

	public CanOnlyMoveOnYourTurnException(String message, Throwable cause) {
		super(message, cause);
	}

	public CanOnlyMoveOnYourTurnException(String message) {
		super(message);
	}

	public CanOnlyMoveOnYourTurnException(Throwable cause) {
		super(cause);
	}
}
