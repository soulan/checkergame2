package org.soen387.domain.model.command.exception;

import org.dsrg.soenea.domain.command.CommandException;

public class CanOnlyRespondToOngoingGameException extends CommandException  {
	private static final long serialVersionUID = -2005725772301540512L;

	public CanOnlyRespondToOngoingGameException() {
		super();
	}

	public CanOnlyRespondToOngoingGameException(String message, Throwable cause) {
		super(message, cause);
	}

	public CanOnlyRespondToOngoingGameException(String message) {
		super(message);
	}

	public CanOnlyRespondToOngoingGameException(Throwable cause) {
		super(cause);
	}
}
