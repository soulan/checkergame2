package org.soen387.domain.model.command.exception;

import org.dsrg.soenea.domain.command.CommandException;

public class CanOnlyConcedeGameIfPlayerIsInvolvedException extends CommandException {
	private static final long serialVersionUID = 3599085869830533242L;

	public CanOnlyConcedeGameIfPlayerIsInvolvedException() {
		super();
	}

	public CanOnlyConcedeGameIfPlayerIsInvolvedException(String message, Throwable cause) {
		super(message, cause);
	}

	public CanOnlyConcedeGameIfPlayerIsInvolvedException(String message) {
		super(message);
	}

	public CanOnlyConcedeGameIfPlayerIsInvolvedException(Throwable cause) {
		super(cause);
	}
}
