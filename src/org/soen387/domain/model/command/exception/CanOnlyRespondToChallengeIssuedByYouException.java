package org.soen387.domain.model.command.exception;

import org.dsrg.soenea.domain.command.CommandException;

public class CanOnlyRespondToChallengeIssuedByYouException extends CommandException {
	private static final long serialVersionUID = 2668853146310148882L;

	public CanOnlyRespondToChallengeIssuedByYouException() {
		super();
	}

	public CanOnlyRespondToChallengeIssuedByYouException(String message, Throwable cause) {
		super(message, cause);
	}

	public CanOnlyRespondToChallengeIssuedByYouException(String message) {
		super(message);
	}

	public CanOnlyRespondToChallengeIssuedByYouException(Throwable cause) {
		super(cause);
	}
}
