package org.soen387.domain.model.command;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.command.exception.CanOnlyRespondToChallengesIssuedAgainstYouException;
import org.soen387.domain.model.command.exception.CanOnlyRespondToOpenChallengesException;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;
import org.soen387.domain.model.challenge.ChallengeStatus;
import org.soen387.domain.model.challenge.IChallenge;
import org.soen387.domain.model.challenge.mapper.ChallengeInputMapper;
import org.soen387.domain.model.checkerboard.CheckerBoardFactory;
import org.soen387.domain.model.notification.NotificationFactory;
import org.soen387.domain.model.notification.NotificationStatus;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;

public class RespondToChallengeCommand extends CheckersCommand{
	public IChallenge challenge;
	public long version;
	public ChallengeStatus status;	

	public RespondToChallengeCommand(Helper helper) {
		super(helper);
	}

	@Override
	public void setUp() throws CommandException {
		try {
			currentPlayer = PlayerInputMapper.find((long)helper.getSessionAttribute("playerid"));
			version = (long)helper.getAttribute("challengeversion");
			status = ChallengeStatus.values()[(int)helper.getAttribute("challengestatus")];
			challenge = ChallengeInputMapper.find((long)helper.getAttribute("challengeid"));
			
		} catch (DomainObjectNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void process() throws CommandException {
		try {
			// Check to see if there is any current player; challengee cannot be current player; Challenge Status is not open
			if(currentPlayer==null) {
				throw new NeedToBeLoggedInException();
			}
			
			challenge.setVersion(version);
			
			if(!challenge.getChallengee().equals(currentPlayer)) {
				throw new CanOnlyRespondToChallengesIssuedAgainstYouException();
			}

			if(!challenge.getStatus().equals(ChallengeStatus.Open)) {
				throw new CanOnlyRespondToOpenChallengesException();
			}
			
			// Change the status from Open to Accepted if all checks pass (depending on the status passed in)
			challenge.setStatus(status);
			if(status.equals(ChallengeStatus.Accepted)) {
				CheckerBoardFactory.createNew(currentPlayer, challenge.getChallenger());
			}
			
			// Change the status from Open to Refused if all checks pass (depending on the status passed in)
			if (status.equals(ChallengeStatus.Refused)) {
				NotificationFactory.createNew(NotificationStatus.Refused, challenge, challenge.getChallenger());
			}
			
			UoW.getCurrent().registerDirty(challenge);
			helper.setRequestAttribute("challenge", challenge);
		} catch (MapperException | SQLException e) {
			throw new CommandException(e);
		}
	
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
