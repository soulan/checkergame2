package org.soen387.domain.model.command;

import java.sql.SQLException;
import java.util.List;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.soen387.domain.model.challenge.ChallengeFactory;
import org.soen387.domain.model.challenge.ChallengeStatus;
import org.soen387.domain.model.challenge.IChallenge;
import org.soen387.domain.model.challenge.mapper.ChallengeInputMapper;
import org.soen387.domain.model.checkerboard.GameStatus;
import org.soen387.domain.model.checkerboard.ICheckerBoard;
import org.soen387.domain.model.checkerboard.mapper.CheckerBoardInputMapper;
import org.soen387.domain.model.command.exception.CannotChallengeSelfException;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;
import org.soen387.domain.model.command.exception.OnlyOneOpenChallengeBetweenPlayersException;
import org.soen387.domain.model.notification.NotificationFactory;
import org.soen387.domain.model.notification.NotificationStatus;
import org.soen387.domain.model.player.IPlayer;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;

public class ChallengePlayerCommand extends CheckersCommand {

	public IPlayer player;
	
	public IChallenge challenge;


	public ChallengePlayerCommand(Helper helper) {
		super(helper);
		try {
			currentPlayer = PlayerInputMapper.find((long)helper.getAttribute("id"));	
			player = PlayerInputMapper.find((long)helper.getAttribute("challengeid"));
		} catch (DomainObjectNotFoundException | SQLException e) {
			System.out.println("COULD NOT FIND PLAYER ID");
			e.printStackTrace();
		}
	}

	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws CommandException {
		try
		{
			//session is not created yet, need to log in before playing the game
			if(currentPlayer==null) {
				throw new NeedToBeLoggedInException();
			}
			
			//can not play with yourself, at least not online :P
			if(player.equals(currentPlayer)) 
			{
				throw new CannotChallengeSelfException();
			}
			
			
			//player has no open challenge with target player
			List<IChallenge> challengerChallenges = ChallengeInputMapper.find(currentPlayer);
			challengerChallenges.retainAll( ChallengeInputMapper.find(player));
			for(IChallenge c: challengerChallenges) {
				if(c.getStatus().equals(ChallengeStatus.Open)) {
					throw new OnlyOneOpenChallengeBetweenPlayersException();
				}
			}
			
			
			//player has no open game with target player
			List<ICheckerBoard> challengeGames = CheckerBoardInputMapper.find(currentPlayer);
			challengeGames.retainAll(CheckerBoardInputMapper.find(player));
			for(ICheckerBoard c: challengeGames) {
				if(c.getStatus().equals(GameStatus.Ongoing)) {
					throw new OnlyOneOpenChallengeBetweenPlayersException();
				}
			}
			
			challenge = ChallengeFactory.createNew(currentPlayer, player);		
			NotificationFactory.createNew(NotificationStatus.Issued, challenge, player);
			helper.setRequestAttribute("challenge", challenge);
		} catch (MapperException | SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("COULD NOT CREATE CHALLENGE");
			e.printStackTrace();
		}
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
