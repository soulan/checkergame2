package org.soen387.domain.model.command;

import java.sql.SQLException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;
import org.soen387.domain.model.notification.Notification;
import org.soen387.domain.model.notification.mapper.NotificationInputMapper;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;

public class DeleteNotificationsCommand extends CheckersCommand {
	long id;

	public DeleteNotificationsCommand(Helper helper) {
		super(helper);
		try {
			currentPlayer = PlayerInputMapper.find((long) helper.getSessionAttribute("playerid"));
			id = (long)helper.getAttribute("notificationid");
		} catch (DomainObjectNotFoundException | SQLException e) {
			System.out.println("COULD NOT FIND PLAYER ID");
			e.printStackTrace();
		}
	}
	
	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws CommandException {
		// player is logged in
		if(currentPlayer==null) {
			throw new NeedToBeLoggedInException();
		}
		
		// Get notification from this currentPlayer
		try { 
			Notification notification = NotificationInputMapper.find(id);
			UoW.getCurrent().registerRemoved(notification);
			helper.setRequestAttribute("notification", notification);
		} catch (MapperException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}
}
