package org.soen387.domain.model.command;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandError;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.domain.helper.Helper;
import org.dsrg.soenea.domain.mapper.DomainObjectNotFoundException;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;
import org.soen387.domain.model.notification.INotification;
import org.soen387.domain.model.notification.mapper.NotificationInputMapper;
import org.soen387.domain.model.player.mapper.PlayerInputMapper;

public class ViewUnseenNotificationsCommand extends CheckersCommand{

	public ViewUnseenNotificationsCommand(Helper helper) {
		super(helper);
		try {
			currentPlayer = PlayerInputMapper.find((long) helper.getSessionAttribute("playerid"));
		} catch (DomainObjectNotFoundException | SQLException e) {
			System.out.println("COULD NOT FIND PLAYER ID");
			e.printStackTrace();
		}
	}

	@Override
	public void setUp() throws CommandException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws CommandException {
		// player is logged in
		if(currentPlayer==null) {
			throw new NeedToBeLoggedInException();
		}
		
		// Get all unseen notifications from this currentPlayer
		try {
			List<INotification> notification;		
			List<INotification> unseen = new ArrayList<INotification>();
			notification = NotificationInputMapper.find(currentPlayer);
			for(INotification n: notification) {
				if(n.getSeen() == false){
					unseen.add(n);
				}
			}
			helper.setRequestAttribute("count", unseen.size());
			helper.setRequestAttribute("notifications", unseen);
		} catch (MapperException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void tearDown() throws CommandError {
		// TODO Auto-generated method stub
		
	}

}
