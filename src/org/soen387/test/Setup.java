package org.soen387.test;

import java.sql.SQLException;

import org.soen387.app.pageControllers.AbstractPageController;
import org.soen387.domain.model.challenge.tdg.ChallengeTDG;
import org.soen387.domain.model.checkerboard.tdg.CheckerBoardTDG;
import org.soen387.domain.model.player.tdg.PlayerTDG;
import org.soen387.domain.model.user.tdg.UserTDG;

public class Setup {

	public static void main(String[] args) throws InterruptedException {
		AbstractPageController.setupDb();
		try {
			CheckerBoardTDG.createTable();
			PlayerTDG.createTable();
			ChallengeTDG.createTable();
			UserTDG.createTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
