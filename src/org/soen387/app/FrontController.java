package org.soen387.app;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dsrg.soenea.service.registry.Registry;
import org.dsrg.soenea.application.filter.Permalink;
import org.dsrg.soenea.application.filter.PermalinkFactory;
import org.dsrg.soenea.application.filter.PermalinkFilter;
import org.dsrg.soenea.application.servlet.DispatcherServlet;
import org.dsrg.soenea.application.servlet.dispatcher.Dispatcher;
import org.dsrg.soenea.domain.helper.Helper;
import org.soen387.domain.model.user.User;
import org.soen387.domain.model.user.mapper.UserOutputMapper;
import org.dsrg.soenea.service.MySQLConnectionFactory;
import org.dsrg.soenea.service.threadLocal.DbRegistry;
import org.dsrg.soenea.service.threadLocal.ThreadLocalTracker;
import org.dsrg.soenea.uow.MapperFactory;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.challenge.Challenge;
import org.soen387.domain.model.challenge.mapper.ChallengeOutputMapper;
import org.soen387.domain.model.checkerboard.CheckerBoard;
import org.soen387.domain.model.checkerboard.mapper.CheckerBoardOutputMapper;
import org.soen387.domain.model.notification.Notification;
import org.soen387.domain.model.notification.mapper.NotificationOutputMapper;
import org.soen387.domain.model.player.Player;
import org.soen387.domain.model.player.mapper.PlayerOutputMapper;
import org.soen387.app.filter.ResourceUrlFilter;
/**
 * Servlet implementation class FrontController
 */
@WebServlet(name="/FrontController", urlPatterns="/Game/*")
public class FrontController extends DispatcherServlet {
	private static final long serialVersionUID = 1L;
	
    public void init() throws ServletException{
    	super.init();
    	prepareDbRegistry();
    	
		//Prepare the unit of work
		MapperFactory myDom = new MapperFactory();
		
		myDom.addMapping(Player.class,PlayerOutputMapper.class);
		myDom.addMapping(User.class,UserOutputMapper.class);
		myDom.addMapping(CheckerBoard.class,CheckerBoardOutputMapper.class);
		myDom.addMapping(Challenge.class,ChallengeOutputMapper.class);
		myDom.addMapping(Notification.class, NotificationOutputMapper.class);
		
		UoW.initMapperFactory(myDom);
    }
    
	public static void prepareDbRegistry() {
		MySQLConnectionFactory f = new MySQLConnectionFactory(null, null, null, null);
		try {
			f.defaultInitialization();
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		DbRegistry.setConFactory(f);
		String tablePrefix;
		try {
			tablePrefix = Registry.getProperty("mySqlTablePrefix");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			tablePrefix = "";
		}
		if(tablePrefix == null) {
			tablePrefix = "";
		}
		DbRegistry.setTablePrefix(tablePrefix);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		preProcessRequest(request,response);
		try{
			processRequest(request,response);
		}	finally{
			postProcessRequest(request,response);
		}
		
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Dispatcher command=getFrontCommand(request); //Get the appropriate commandClass from the passed parameter.
		if(command==null){
			System.out.println("Unknown Command Passed in the URL");
		}else{
			command.init(request, response);
			command.execute();
		}
	}
	
	private Dispatcher getFrontCommand(HttpServletRequest request) {
		try{
			//Creates a new permalink Factory Class
			PermalinkFactory pl=new PermalinkFactory();
			String command=pl.extract(request);  //This line Extracts the class defined in the Permalink file.
			return (Dispatcher) Class.forName(command).newInstance();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
	
	protected void preProcessRequest(HttpServletRequest request, HttpServletResponse response){
		try{
			
			
			DbRegistry.getDbConnection().createStatement().execute("Start Transaction");
			UoW.newCurrent();

			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	protected void postProcessRequest(HttpServletRequest request, HttpServletResponse response){
		try{
			UoW.getCurrent().commit(); //This is where the unit of work will commit everything
			DbRegistry.closeDbConnectionIfNeeded(); //- connections must be closed
			ThreadLocalTracker.purgeThreadLocal(); // Purge Threads
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
