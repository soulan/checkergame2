package org.soen387.app.pageControllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.role.IRole;
import org.dsrg.soenea.service.threadLocal.DbRegistry;
import org.soen387.domain.model.player.Player;
import org.soen387.domain.model.player.mapper.PlayerOutputMapper;
import org.soen387.domain.model.player.tdg.PlayerFinder;
import org.soen387.domain.model.role.PlayerRole;
import org.soen387.domain.model.user.User;
import org.soen387.domain.model.user.mapper.UserOutputMapper;
import org.soen387.domain.model.user.tdg.UserFinder;

/**
 * Servlet implementation class ListGames
 */
@WebServlet("/RegisterPlayer")
public class RegisterPlayer extends AbstractPageController implements Servlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see AbstractPageController#AbstractPageController()
     */
    public RegisterPlayer() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		setupRequest(request);
		
		try {
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			
			List<IRole> role = new ArrayList<IRole>();
			PlayerRole e = new PlayerRole();
			role.add(e);
			
			User u = new User(UserFinder.getMaxId(), 1, username, password);
			Player p = new Player(PlayerFinder.getMaxId(), 1, firstName, lastName, email, u, role);
			
			UserOutputMapper uom = new UserOutputMapper();
			PlayerOutputMapper pom = new PlayerOutputMapper();
			
			try {
				uom.insert(u);
				pom.insert(p);
			} catch (MapperException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.out.println("ERROR REGISTER PLAYER INSERT!");
			}
			request.setAttribute("player", p);
			
			//Commit
			DbRegistry.getDbConnection().commit();
			
			request.getRequestDispatcher("/WEB-INF/jsp/xml/player.jsp").forward(request, response);
		} catch (SQLException e) {
			forwardError(request, response, e.getMessage());
			e.printStackTrace();
		} finally {
			teardownRequest();	
		}
		
		
		
	}


}
