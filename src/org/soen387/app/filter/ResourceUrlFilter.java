package org.soen387.app.filter;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.dsrg.soenea.application.filter.PermalinkFilter;

@WebFilter("/Game/*")
public class ResourceUrlFilter extends PermalinkFilter {

	
	public void doFilter(ServletRequest request, ServletResponse response,
		FilterChain chain) throws IOException, ServletException{
		getAttributes(request);
		chain.doFilter(request, response);
	}
	
	public static void getAttributes(ServletRequest request){
		
		String path=((HttpServletRequest) request).getPathInfo();
	
		Pattern Board=Pattern.compile("Board/(\\d*)");
		Pattern Move=Pattern.compile("Board/(\\d*)/((\\w+\\s*)*)");
		Pattern Player=Pattern.compile("Player/(\\d*)");
		Pattern Challenge=Pattern.compile("Challenge/(\\d*)");
		Pattern Withdraw=Pattern.compile("Challenge/(\\d*)/(\\d*)");
		Pattern Respond=Pattern.compile("Challenge/(\\d*)/(\\d*)/(\\d*)");
		Pattern Login=Pattern.compile("LogIn/(\\w*)/(\\w*)");
		Pattern Register=Pattern.compile("Register/(\\w*)/(\\w*)/(\\w*)/(\\w*)/(\\w*@\\w*\\.\\w*)");
		Pattern See=Pattern.compile("Notification/(\\d+)");
		
		Matcher match= Board.matcher(path);
		if(match.find()){
			String BoardIdString=match.group(1);
			long BoardId=Long.parseLong(BoardIdString);
			request.setAttribute("boardid",BoardId);
		}
		
		match= Move.matcher(path);
		if(match.find()){
			String BoardIdString=match.group(1);
			String PiecesString = match.group(2);
			long BoardId=Long.parseLong(BoardIdString);
			request.setAttribute("boardid",BoardId);
			request.setAttribute("pieces", PiecesString);
		}
		
		match=Player.matcher(path);
		if(match.find()){
			String PlayerIdString=match.group(1);
			long PlayerId=Long.parseLong(PlayerIdString);
			request.setAttribute("id",PlayerId);
		}
		
		match=Challenge.matcher(path);
		if(match.find()){
			String ChallengeIdString=match.group(1);
			long ChallengeId=Long.parseLong(ChallengeIdString);
			request.setAttribute("challengeid",ChallengeId);
		}
		
		match=Withdraw.matcher(path);
		if(match.find()){
			String ChallengeIdString=match.group(1);			
			String ChallengeVersionString = match.group(2);
			long ChallengeId=Long.parseLong(ChallengeIdString);
			long ChallengeVersion=Long.parseLong(ChallengeVersionString);
			request.setAttribute("challengeid",ChallengeId);	
			request.setAttribute("challengeversion",ChallengeVersion);
		}
		
		match=Respond.matcher(path);
		if(match.find()){
			String ChallengeIdString=match.group(1);			
			String ChallengeVersionString = match.group(2);
			String ChallengeStatusString = match.group(3);
			long ChallengeId=Long.parseLong(ChallengeIdString);
			long ChallengeVersion=Long.parseLong(ChallengeVersionString);
			int ChallengeStatus=Integer.parseInt(ChallengeStatusString);
			request.setAttribute("challengeid",ChallengeId);	
			request.setAttribute("challengeversion",ChallengeVersion);
			request.setAttribute("challengestatus",ChallengeStatus);
		}
		
		match=Login.matcher(path);
		if(match.find()){
			String username=match.group(1);
			String password=match.group(2);
			request.setAttribute("username", username);
			request.setAttribute("password", password);
		}
		
		match=Register.matcher(path);
		if(match.find()){
			String username=match.group(1);
			String password=match.group(2);
			String firstName=match.group(3);
			String lastName=match.group(4);
			String email=match.group(5);
			request.setAttribute("username", username);
			request.setAttribute("password",password);
			request.setAttribute("firstName",firstName);
			request.setAttribute("lastName",lastName);
			request.setAttribute("email",email);
		}
		
		match=See.matcher(path);
		if(match.find()){
			String NotificationIdString=match.group(1);
			long NotificationId=Long.parseLong(NotificationIdString);
			request.setAttribute("notificationid",NotificationId);
		}
	}
	
}