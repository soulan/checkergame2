package org.soen387.app.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;

import org.soen387.domain.model.command.SeeNotificationsCommand;

public class SeeNotificationsDispatcher extends CheckersDispatcher {

	@Override
	public void execute() throws ServletException, IOException {
		try {
			new SeeNotificationsCommand(myHelper).execute();
			forward("seenotification.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

}
