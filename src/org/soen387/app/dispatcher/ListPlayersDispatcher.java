package org.soen387.app.dispatcher;
import java.io.IOException;

import javax.servlet.ServletException;

import org.dsrg.soenea.domain.command.CommandException;
import org.soen387.domain.model.command.ListPlayersCommand;
public class ListPlayersDispatcher extends CheckersDispatcher{

	@Override
	public void execute() throws ServletException, IOException {
		try {
			new ListPlayersCommand(myHelper).execute();
			forward("players.jsp");
		} catch (CommandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	} 

}
