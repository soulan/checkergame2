package org.soen387.app.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;

import org.soen387.domain.model.command.DeleteAccountCommand;

public class DeleteAccountDispatcher extends CheckersDispatcher {

	@Override
	public void execute() throws ServletException, IOException {
		try {
			new DeleteAccountCommand(myHelper).execute();
			myHelper.invalidateSession();
			forward("success.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
