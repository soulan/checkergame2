package org.soen387.app.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;

import org.dsrg.soenea.domain.command.CommandException;
import org.soen387.domain.model.command.LogInCommand;
import org.soen387.domain.model.command.exception.NoSuchUserException;

public class LogInDispatcher extends CheckersDispatcher{

	@Override
	public void execute() throws ServletException, IOException {
		System.out.println("hello from the "+ "Login" + "Dispatcher");
		
		try {
			new LogInCommand(myHelper).execute();
			forward("player.jsp");
		} catch (final NoSuchUserException e) {
			fail("No such username and password combination.");
		} catch (final CommandException e) {
			fail(e);
		}
	}

}
