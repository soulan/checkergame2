package org.soen387.app.dispatcher;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.command.ConcedeGameCommand;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;

public class ConcedeGameDispatcher extends CheckersDispatcher {
	public void execute() throws ServletException, IOException {
		System.out.println("hello from the "+ "WithdrawChallenge" + "Dispatcher");
		
		if(true) { //isSubmission) {
			try {
				new ConcedeGameCommand(myHelper).execute();
				// Must commit to see that the game is over
				UoW.getCurrent().commit();
				forward("concedegame.jsp");
			} catch (final NeedToBeLoggedInException e) {
				fail("You need to be logged in to challenge a player.");
			} catch (final CommandException e) {
				fail(e);
			} catch (InstantiationException | IllegalAccessException | MapperException | SQLException e) {
				fail(e); //UoW went to crap 
			}
		}
	}
}
