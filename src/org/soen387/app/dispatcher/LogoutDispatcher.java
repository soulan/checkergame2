package org.soen387.app.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;

public class LogoutDispatcher extends CheckersDispatcher {

	@Override
	public void execute() throws ServletException, IOException {
		System.out.println("hello from the "+ "Logout" + "Dispatcher");
		
		// No Command for logout
		myHelper.invalidateSession();
		forward("success.jsp");
	}

}
