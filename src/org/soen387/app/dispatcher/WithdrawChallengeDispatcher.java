package org.soen387.app.dispatcher;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;

import org.dsrg.soenea.domain.MapperException;
import org.dsrg.soenea.domain.command.CommandException;
import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.command.WithdrawChallengeCommand;
import org.soen387.domain.model.command.exception.NeedToBeLoggedInException;

public class WithdrawChallengeDispatcher extends CheckersDispatcher {

	@Override
	public void execute() throws ServletException, IOException {
		System.out.println("hello from the "+ "WithdrawChallenge" + "Dispatcher");
		
		if(true) { //isSubmission) {
			try {
				new WithdrawChallengeCommand(myHelper).execute();
				// must commit to see the withdrawal
				UoW.getCurrent().commit(); 
				forward("success.jsp");
			} catch (final NeedToBeLoggedInException e) {
				fail("You need to be logged in to challenge a player.");
			} catch (final CommandException e) {
				fail(e);
			} catch (InstantiationException | IllegalAccessException | MapperException | SQLException e) {
				fail(e); //UoW went to crap 
			}
		}
	}
	
}
