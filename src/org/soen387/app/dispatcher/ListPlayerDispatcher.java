package org.soen387.app.dispatcher;
import java.io.IOException;

import javax.servlet.ServletException;

import org.dsrg.soenea.domain.command.CommandException;
import org.soen387.domain.model.command.ListPlayersCommand;
public class ListPlayerDispatcher extends CheckersDispatcher{

	@Override
	public void execute() throws ServletException, IOException {
		System.out.println("hello from the "+ "ListPlayer" + "Dispatcher");
		
		try {
			new ListPlayersCommand(myHelper).execute();
			forward("players.jsp");
		} catch (final CommandException e) {
			fail(e);
		}
	} 

}
