package org.soen387.app.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;

import org.soen387.domain.model.command.DeleteNotificationsCommand;

public class DeleteNotificationsDispatcher extends CheckersDispatcher {

	@Override
	public void execute() throws ServletException, IOException {
		try {
			new DeleteNotificationsCommand(myHelper).execute();
			forward("deletenotification.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

}
