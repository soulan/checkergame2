package org.soen387.app.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;

import org.soen387.domain.model.command.ViewAllNotificationsCommand;

public class ViewAllNotificationsDispatcher extends CheckersDispatcher {

	@Override
	public void execute() throws ServletException, IOException {
		try {
			new ViewAllNotificationsCommand(myHelper).execute();
			forward("viewallnotifications.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

}
