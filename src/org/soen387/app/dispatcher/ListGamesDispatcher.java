package org.soen387.app.dispatcher;
import java.io.IOException;
import javax.servlet.ServletException;

import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.command.ListGamesCommand;
public class ListGamesDispatcher extends CheckersDispatcher {

	@Override
	public void execute() throws ServletException, IOException {
		
			try {
				new ListGamesCommand(myHelper).execute();
				UoW.getCurrent().commit(); //Do we need to commit when only listing games?
				forward("listgames.jsp");
			} catch (Exception e) {
				e.printStackTrace();
			}		
	}

}
