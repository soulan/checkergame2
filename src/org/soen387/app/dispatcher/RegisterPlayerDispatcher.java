package org.soen387.app.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;

import org.dsrg.soenea.uow.UoW;
import org.soen387.domain.model.command.LogInCommand;
import org.soen387.domain.model.command.RegisterPlayerCommand;
import org.soen387.domain.model.command.exception.NoSuchUserException;

public class RegisterPlayerDispatcher extends CheckersDispatcher {

	@Override
	public void execute() throws ServletException, IOException {
		System.out.println("hello from the "+ "RegisterPlayer" + "Dispatcher");
		
		try {
			new RegisterPlayerCommand(myHelper).execute(); System.out.println("--REGISTERED USER");
			
			// Must commit so that the user will be logged in after registering
			UoW.getCurrent().commit();
			
			new LogInCommand(myHelper).execute();System.out.println("--LOGIN");
			System.out.println();
			forward("player.jsp");
		} catch (final NoSuchUserException e) {
			fail("No such username and password combination.");
		} catch (final Exception e) {
			e.printStackTrace();
			fail(e);
		} 
	}

}
