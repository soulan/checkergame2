package org.soen387.app.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;

import org.dsrg.soenea.domain.command.CommandException;
import org.soen387.domain.model.command.ListChallengesCommand;

public class ListChallengesDispatcher extends CheckersDispatcher{

	@Override
	public void execute() throws ServletException, IOException {
		System.out.println("hello from the "+ "ListChallenge" + "Dispatcher");
		
		try {
			new ListChallengesCommand(myHelper).execute();
			forward("challenges.jsp");
		} catch (final CommandException e) {
			fail(e);
		}
	}

}
