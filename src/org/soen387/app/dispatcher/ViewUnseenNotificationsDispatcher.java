package org.soen387.app.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;

import org.soen387.domain.model.command.ViewUnseenNotificationsCommand;

public class ViewUnseenNotificationsDispatcher extends CheckersDispatcher {

	@Override
	public void execute() throws ServletException, IOException {
		try {
			new ViewUnseenNotificationsCommand(myHelper).execute();
			forward("viewunseennotifications.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

}
