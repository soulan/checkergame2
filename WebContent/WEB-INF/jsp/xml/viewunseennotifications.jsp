<%@ page trimDirectiveWhitespaces="true" %>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml version="1.0" encoding="UTF-8"?>
<checkers>
<status>success</status>
<notifications count="${count }">
	<c:forEach var="notification" items="${notifications}">
	<c:if test="${notification.status.id == 0 }">
		<turn id="${notification.id }" version="${notification.version }" recipient="${notification.recipient.user.id }" seen="${notification.seen }" game="${notification.type }" />
	</c:if>
	<c:if test="${notification.status.id == 1 }">
		<tied id="${notification.id }" version="${notification.version }" recipient="${notification.recipient.user.id }" seen="${notification.seen }" game="${notification.type }" />
	</c:if>
	<c:if test="${notification.status.id == 2 }">
		<won id="${notification.id }" version="${notification.version }" recipient="${notification.recipient.user.id }" seen="${notification.seen }" game="${notification.type }" />
	</c:if>
	<c:if test="${notification.status.id == 3 }">
		<conceded id="${notification.id }" version="${notification.version }" recipient="${notification.recipient.user.id }" seen="${notification.seen }" game="${notification.type }" />
	</c:if>
	<c:if test="${notification.status.id == 4 }">
		<loss id="${notification.id }" version="${notification.version }" recipient="${notification.recipient.user.id }" seen="${notification.seen }" game="${notification.type }" />
	</c:if>
	<c:if test="${notification.status.id == 5 }">
		<started id="${notification.id }" version="${notification.version }" recipient="${notification.recipient.user.id }" seen="${notification.seen }" game="${notification.type }" />
	</c:if>
	<c:if test="${notification.status.id == 6 }">
		<issued id="${notification.id }" version="${notification.version }" recipient="${notification.recipient.user.id }" seen="${notification.seen }" challenge="${notification.type }" />
	</c:if>
	<c:if test="${notification.status.id == 7 }">
		<accepted id="${notification.id }" version="${notification.version }" recipient="${notification.recipient.user.id }" seen="${notification.seen }" challenge="${notification.type }" />
	</c:if>
	<c:if test="${notification.status.id == 8 }">
		<refused id="${notification.id }" version="${notification.version }" recipient="${notification.recipient.user.id }" seen="${notification.seen }" challenge="${notification.type }" />
	</c:if>
	</c:forEach>
</notifications>
</checkers>