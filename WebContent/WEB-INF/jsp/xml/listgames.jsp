<%@ page trimDirectiveWhitespaces="true" %>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml version="1.0" encoding="UTF-8"?>
<checkers>
<status>success</status>
 <games>
 <c:forEach var="game" items="${games}">
  <game id="${game.id}" version="${game.version}" status="${game.status}">
   <firstPlayer refid="${game.firstPlayer.user.id}" />
   <secondPlayer refid="${game.secondPlayer.user.id}" />
   <currentPlayer refid="${game.currentPlayer.user.id}" />
  </game>
</c:forEach>
 </games>
</checkers>